import bodyParser from 'body-parser'
import session from 'express-session'
import app from './app'

module.exports = {
  mode: 'spa',
  srcDir: '',
  generate: {
    dir: 'wwwroot',
    routes: [
      // Generate static pages for static file servers handling dynamic routes
    ]
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'src',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'client' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },

  // api middleware
  serverMiddleware: [
    // body-parser middleware
    // bodyParser.json(),
    // session middleware
    session({
      store: new app.pgSession({ conObject: app.db.conObject, tableName: 'sessions' }),
      secret: 'cAtOnTheBoaRd',
      resave: true,
      saveUninitialized: false,
      cookie: { secure: true, maxAge: 30 * 24 * 60 * 60 * 1000 }
    }),

    '~/api/', '~/api/auth/'],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'token' },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/user', method: 'get', propertyName: 'user' }
        },
        tokenRequired: true,
        tokenType: 'bearer'
        // tokenRequired: true,
        // tokenType: 'bearer'
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

