﻿const app = require('../../app');

app.post('/login', async (req, res, next) => {

  let promRez = app.db.one('SELECT id, mail FROM users WHERE mail = $1 AND pass = $2 LIMIT 1', [req.body.username, req.body.password]);

  // console.log(promRez)

  try {
    let rez = await promRez;

    // console.log(rez)

    req.session.authUser = { username: req.body.username, userid: rez.id }
    return res.json({ username: req.body.username, token: req.sessionID })
  }
  catch (e) {
    return res.status(401).json({ message: 'Bad credentials' + e })
  }

});

app.post('/logout', (req, res, next) => {
  res.send('API logout')
});

app.get('/user', async (req, res, next) => {
  res.send('API user')
});

// export the server middleware
module.exports = {
  path: '/api/auth',
  handler: app
}