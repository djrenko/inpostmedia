const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const session = require('express-session')
const SHA256 = require("crypto-js/sha256");
const BASE64 = require('base64-js');
const db = require('./db');
const bodyParser = require('body-parser')
const pgSession = require('connect-pg-simple')(session);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))


app.use(cookieParser());

// app.set('trust proxy', 1) // trust first proxy
// app.use(session({
//   store: new pgSession({ conObject: db.conObject, tableName: 'sessions' }),
//   secret: 'cAtOnTheBoaRd',
//   resave: true,
//   saveUninitialized: true,
//   cookie: { secure: true, maxAge: 30 * 24 * 60 * 60 * 1000 }
// }));


app.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})



module.exports = Object.assign({}, app, { db: db, pgSession: pgSession });

