﻿const pgp = require("pg-promise")(/*options*/);
const db = pgp("postgres://postgres:postgres@localhost:5432/inpostmedia");

// console.log(db.$pool);

db.one("SELECT 'TEST SELECT $1 ' TESTCOL", [123])
  .then(function (data) {
    console.log("DATA:", data);
  })
  .catch(function (error) {
    console.log("ERROR:", error);
  });

  const conObject = {
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'postgres',
    database: 'inpostmedia'
};


module.exports = Object.assign({}, db, {conObject: conObject});