# src

> client

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).






<!-- POSTGRES SCRIPTS -->

<!-- --ALTER USER postgres WITH PASSWORD 'postgres'; -->
<!-- --CREATE TABLE "session" ( -->
<!-- --  "sid" varchar NOT NULL COLLATE "default", -->
<!-- --	"sess" json NOT NULL, -->
<!-- --	"expire" timestamp(6) NOT NULL -->
<!-- --) -->
<!-- --WITH (OIDS=FALSE); -->
<!-- --ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE; -->


<!-- -- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    mail character varying(255) COLLATE pg_catalog."default" NOT NULL,
    insert_date timestamp with time zone NOT NULL DEFAULT now(),
    CONSTRAINT users_pkey PRIMARY KEY (id, mail, insert_date)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres; -->

<!-- 
ALTER TABLE public.users
ADD COLUMN pass character varying(255) NOT NULL; 
-->

<!-- 
INSERT INTO public.users(
	 mail, insert_date, pass)
	VALUES ('vlad@gmail.com', now(), 'pass'); -->

<!--  -->