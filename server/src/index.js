const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const app = express()
const i18n = require("i18n")

const config = require('../config/config')

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

i18n.configure({
    locales: ['en', 'ru'],
    directory: __dirname + '/i18n'
});
app.use(i18n.init)

const mainRouter = require('./routers/mainRouter')

app.use('/main', mainRouter)

app.listen(process.env.PORT || config.port,
    () => console.log(`Server start on port ${config.port} ...`))