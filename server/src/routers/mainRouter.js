﻿var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.get('/t', function(req, res) {
    res.send(res.locals.__(req.query.q) || '');
});

router.get('/about', function(req, res) {
    res.send('About Main Router');
});

module.exports = router;